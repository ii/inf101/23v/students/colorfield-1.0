package no.uib.inf101.sample;

import no.uib.inf101.sample.eventbus.EventBus;
import no.uib.inf101.sample.eventbus.EventHandler;

import java.awt.*;
import java.util.Random;

public class ColorFieldModel implements ViewableColorField {

  private Color color;
  private EventBus bus;
  private Random random;

  private static final int COLOR_VALUE_BOUND = 256;

  /** Create a new ColorFieldModel. */
  public ColorFieldModel(Random random) {
    this.random = random;
    this.bus = new EventBus();
    this.color = this.generateRandomColor();
  }

  /** Sets the current color of the field to a new random color. */
  public void disruptWithNewColor() {
    this.color = this.generateRandomColor();
    this.bus.post(new ColorFieldModelChangedEvent(this, this.color));
  }

  private Color generateRandomColor() {
    return new Color(
        this.random.nextInt(COLOR_VALUE_BOUND),
        this.random.nextInt(COLOR_VALUE_BOUND),
        this.random.nextInt(COLOR_VALUE_BOUND)
    );
  }

  @Override
  public void registerForEvents(EventHandler listener) {
    this.bus.register(listener);
  }

  @Override
  public Color getCurrentColor() {
    return this.color;
  }
}
