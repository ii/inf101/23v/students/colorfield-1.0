package no.uib.inf101.sample;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;

public class ColorFieldController extends MouseAdapter {

  private ColorFieldModel model;
  private ColorFieldView view;

  /** Create a new controller for the given model and view. */
  public ColorFieldController(ColorFieldModel model, ColorFieldView view) {
    this.model = model;
    this.view = view;
    this.view.addMouseListener(this);
  }

  @Override
  public void mousePressed(MouseEvent e) {
    super.mousePressed(e);
    Ellipse2D circle = this.view.getCircle();
    if (circle.contains(e.getPoint())) {
      this.model.disruptWithNewColor();
    }
  }
}
